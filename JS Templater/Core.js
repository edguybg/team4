/**
 * Basic Template Engine
 */
var JSTepmplater = function () {

    //////////////////
    // private vars //
    //////////////////
    var regexCode = '(<%.+?%>)';
    var regexEcho = '<%=\\s*([a-z0-9_]*)((?:\\[(?:\\d+|[\'\"][a-z0-9_]*[\'\"])\\])*);\\s*%>';
    var object;

    ///////////////////////
    // PRIVATE FUNCTIONS //
    ///////////////////////

    /**
     * Identifies type of template code and returns data depending on it.
     * @return {String} Value
     */
    function identify () {

        // is code an 'echo'
        if (arguments[1].indexOf('<%=') === 0) {
            return arguments[1].replace(new RegExp(regexEcho, 'gi'), echo);
        }

        return warning('Invalid sintax:' + arguments[1]);
    }

    /**
     * Function used to replace echo (<%=) code blocks with values from object.
     * @return {String} Value
     */
     function echo () {
        var obj = object[arguments[1]]; // key

        if (arguments[2] !== '') { // if there are keys to the object key, splits them and sets obj to needed object object
            var keys = arguments[2].substr(1, arguments[2].length - 2).split(/(?:['"]*\]\[['"]*)/);

            for (var i = 0, j = keys.length - 1; i < j; i++) {

                if (obj[keys[i]] === undefined) {
                    return undefined;
                }

                obj = obj[keys[i]];
            }

            obj = obj[keys[keys.length - 1]];
        }

        if (obj === undefined) {
            return warning('Value not found: ' + arguments[1] + arguments[2]);
        }

        if (typeof obj === 'object') {
            return warning('Value of type object being returned! Are you missing a key?');
        }


        return obj;
    }

    /**
     * Generates a warning message string
     * @param  {String} message Message
     * @return {String}         Warning
     */
    function warning (message) {
        return '<b>JSTepmplater Warning!</b><br><i style="color:red;">' + message + '</i>';
    }

    ////////////////
    // PROPERTIES //
    ////////////////

    /**
     * If False, returns exceptions instead of objects.
     * @type {Boolean}
     */
    this.regurnObjects = false;

    ////////////////////////
    // PRIVILEGED METHODS //
    ////////////////////////

    /**
     * Replaces template code in given text into corresponding values from given data.
     * @param  {String} text Text with template code
     * @param  {Object} data Data to be used
     * @return {String}      Text with entered values
     */
    this.load = function (text, data) {
        object = data;
        var regex = new RegExp(regexCode, 'gi');
        var matches;

        text = text.replace(regex, identify);

        return text;
    };
};