/**
 * Creates an HTTP Request
 *
 * @param  {String} url  Url to file
 * @param  {Function} func Function to be called on ready
 *
 * @return void
 */
function createHTMLRequest (url, func) {
    var xmlhttp;

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            func(xmlhttp.responseText);
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

/**
 * Parses array of objects as a table
 *
 * @param  {Array}   objArray      Array of objects
 * @param  {Boolean} includeEvents Include events to re-sort by header
 *
 * @return {String}                Table HTML
 */
function parseTable (objArray, includeEvents) {

    if (includeEvents === undefined || typeof includeEvents !== 'boolean')
    {
        includeEvents = true;
    }

    var tableHeaders = [],
        tableRows = [],
        checkHeaders = true;

    /**
     * Checks if given parameter is of type 'object'
     *
     * @param  {Mixed}   obj Parameter to be checked
     *
     * @return {Boolean}     TRUE if parmeter is an object, otherwise False
     */
    function isObject (obj) {
        return obj !== null && typeof obj === 'object';
    }

    /**
     * Parses an object as a table row
     *
     * @param  {Object} obj Object to be parsed
     *
     * @return {String}     Table row HTML
     */
    function getEntry (obj) {
        var row = '<tr>';

        for (var key in obj) {
            row += '<td>';

            if (checkHeaders) {
                if(tableHeaders.indexOf(key) === -1) {
                    tableHeaders.push(key);
                }
            }

            if (isObject(obj[key])) {
                row += parseTable([obj[key]], false);
            } else {
                row += obj[key];
            }

            row += '</td>';
        }

        row += '</tr>';

        return row;
    }

    // get all table rows
    objArray.forEach(function (obj) {
        tableRows.push(getEntry(obj));
        checkHeaders = false;
    });

    // start preparing result
    var result = '<table border=1><tr>';

    // add headers
    tableHeaders.forEach(function (header) {
      result += '<th';

      if (includeEvents)
      {
         result +=' onclick="sortByHeader(this);"';
      }

      result += '>' + header + '</th>';
    });

    // add all rows
    result += '</tr>' + tableRows.join('') + '</table>';

    return result;
}

/**
 * Object, holding sorting functionality
 *
 * @type {Object}
 */
var Sort = {
    /**
     * Function to sort Array of Object
     *
     * @param {String} key  Object key by which array will be sorted
     * @param {Array}  data Array
     *
     * @return void
     */
    Objects: function (key, data) {
        data.sort(function (a, b) {

            if (typeof a[key] === 'number') {
                return a[key] - b[key];
            } else if (typeof a[key] === 'string') {
                return a[key].localeCompare(b[key]);
            }

            return 0;
        });
    }
};

/**
 * Gets inner HTML and sorts given array
 *
 * @param  {Object} header DOM Object whose inner html will be used
 *
 * @return void
 */
function sortByHeader (header) {
   var key = header.innerHTML;
   console.log(object);
   Sort.Objects(key, object);
   document.querySelector('header').innerHTML = parseTable(object);
}

/**
 * Global object to hold AJAX information
 *
 * @type {Array}
 */
var object = null;

(function() {
    createHTMLRequest('scripts/ajax.json', function (text) {
        object = JSON.parse(text);
        Sort.Objects('name', object);
        document.querySelector('header').innerHTML = parseTable(object);
    });
})();