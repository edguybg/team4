//Всички числа са Number, няма int, float, double. ...

Методите на числата не са имплементирани в самия тип number, всички са в отделна библиотека - Math

String - пак е неизменим

var a = 5;

Math.floor(4,5) //4

Math.max(4,5) //5

var b = "str";

b.charAt();

10 + '10' // = '1010'

'1' + 10 - '1'  // 109

parseInt('100') // 100

function maxNumber (n1, n2) {
    return Math.max(n1, n2);
}

// Деклариране на функция и на променлива - функцията е с предимство, така може да се ползват функции преди променливи

var funcA;
var funcB;

funcA = function () {}; // анонимна функция , която е аssing-aта на Object

funcB = function a () {
    // въпреки, че е именована фукнцията, тя не може да се присвои на нищо друго, тя е непосредствената стойност
    //ако се иска нейния резултат тр да се ползва funcB
};

function b () {
    //именована функция, ако няма 'б' няма начин да извикаме в последствие дадената функция
};



(function () {
    //анонимна функция, която се извиква веднага след декларирането, не чакаме определено събитие, "самоизвикваща функция"
    //immediate invoking function execution = iife
})()

//Задача 2: Функция - при подадени три числа да се връща най-голямото от тях


function maxOfThree(n1, n2, n3){
    var max = n1;

    if(n2 > n1){
        max = n2;
    }
    if(n3 > n2){
        max = n3;
    }

    return max;
}


//Задача 3 - Write a function that takes a character (i.e. a string of length 1) and returns true if it is a vowel, false otherwise.
function vowelCheck(str){

    var vowels = ['a', 'o', 'e', 'i', 'y', 'u'];

    if(str.length==1 && vowels.indexOf(str.toLowerCase()) >=0){
        return true;
    }

    return false;
}

// Задача 4

function translate(str){

    var result = '';

    for (var i = 0; i < str.length; i++) {
        if(!vowelCheck(str[i]) && str[i].match(/[a-z]/i)){
            result += str[i] + 'o' + str[i];
        } else {
            result += str[i];
        }
    }
}


function translate(string){
    var result = [],
    vowels = 'aeoiu';

    string.split('').forEach(function(character){
        if(vowels.indexOf(character) === -1 && character.match(/[a-z]/i)){
            character = character + 'o' + character;
        }

        result.push(character);
    });

    return result.join('');
}

// задача 5
function sum(arr){
    var result = 0;

    for (var i = 0; i < arr.length; i++) {
        result = parseInt(arr[i]);
    }
}

function multiply(arr){
    var result = 0;

    for(var i=0; i < arr.length; i++){
        result *=
    }
}


var obj = {
    "merry":"god",
    "christmas":"jul",
    "and":"och",
    "happy":"gott",
    "new":"nytt",
    "year":"år"
}

//Прилича на key-value Pair, key може да е без кавички
// Достъпване на елементи:
obj.merry // god
obj.christmas // jul
obj[christmas]  // uncaught
obj['christmas'] // jul
obj['merry'] // god

christmas = function () { return "year"}
obj[christmas()] // år


Object.definedProperty() //ползва се единствено и само нашето пропърти да не може да бъде достъпвано, променяно, изкл от or, and

delete obj.newProp // не изчезва реално


//HW

//Задача 6
function reverse(string){
    var result = '',
        array = string.split('');
    for (var i = array.length - 1; i >= 0; i--) {
        result += array[i]
    }

    return result;
}

//Задача 7
function bilingualLexicon(){
    var greeting = 'Merry christmas and happy new year',
        result = [],
        array = greeting.toLowerCase().split(' '),
        dic =
    {
        "merry":"god",
        "christmas":"jul",
        "and":"och",
        "happy":"gott",
        "new":"nytt",
        "year":"år"
    }

    array.forEach( function (word) {
            result.push(dic[word]);
    });

    return result.join(' ');
}

//Задача 8
function findLongestWord(string){
    var array = string.split(' '),
        longestWord = '';

    array.forEach(function (word){
        if(word.length > longestWord.length){
            longestWord = word;
        }
    })

    return longestWord;
}

//Задача 9
function filterLongWords (string, i) {
    var array = string.split(' '),
        filteredWords = [];

    array.forEach(function (word){
        if(word.length > i){
            filteredWords.push(word);
        }
    })

    return filteredWords;
}

//Задача 10
function charFreq (string) {
    var result = {};

    string.split('').forEach(function (element) {
        if(result[element] === undefined){
            result[element] = 1;
        } else {
            result[element] += 1;
        }
    });

    return result;
}

//Задача 11
function checkValue (value) {
    var result = value;

    if(value === 1000000){
        result += ' dollars ($$$)';
    } else {
        result += ' leva';
    }

    return result;
}

//Задача 12
function mixUp(str1, str2){
    var result = '';

    if(str1.length <=2 && str2.length <=2){
        result = "Too short strings, try again =)";
    } else {
        result = str2.substring(0, 2) + str1.substring(2) + ' ' + str1.substring(0, 2) + str2.substring(2);
    }

    return result;
}

//задача 13
function fixStart (string) {
    var result = '';

    if(string.length > 1){
        var firstLetter = string[0];
        result += firstLetter;
        for (var i = 1; i < string.length; i++) {
            if(string[i] === firstLetter){
                result += '*';
            } else {
                result += string[i];
            }
        }
    }

    return result;
}

//Задача 14
function verbing (string) {
    var result = string;

    if(string.length > 3){
        var substringIndex = string.length - 3;
        var ending = string.substring(substringIndex);
        if(ending === 'ing'){
            result += 'ly';
        } else {
            result += 'ing';
        }
    }

    return result;
}
