/**
 * Returns largest of two numbers
 *
 * @param  {Number} a First number
 * @param  {Number} b Second number
 *
 * @return {Number}   Returns largest number
 */
function max (a, b) {
    return Math.max(a,b);
}

/**
 * Returns largest of three numbers
 *
 * @param  {Number} a First number
 * @param  {Number} b Second number
 * @param  {Number} c Third number
 *
 * @return {Number}   Largest number
 */
function maxOfThree (a, b, c) {
    return Math.max(a, b, c);
}

/**
 * Checks if given character is a vowel
 *
 * @param  {String}  character Single vowel
 *
 * @return {Boolean}           Returns TRUE if given character is a vowel
 */
function isVowel (character) {
    return character.match(/[aoueyi]/i).length > 0;
}

/**
 * Translates text into "rövarspråket".
 * That is, double every consonan and place and occurence of "o" in between.
 *
 * @param  {String} str Text to be "translated"
 *
 * @return {String}     Translated text
 */
function translate (str) {
    return str.replace(/(?![aoueiy])[a-z]/gi, '$&o$&');
}

/**
 * Sums all numbers in given array.
 *
 * @param  {Array}  arr Array with numbers
 *
 * @return {Number}     Returns sum of numbers (0 if no numbers in array).
 */
function sum (arr) {
    var result = 0;

    arr.forEach(function (num) {
        result += (isNaN(parseInt(num))) ? 0 : parseInt(num);
    });

    return result;
}

/**
 * Multiplies numbers in array
 *
 * @param  {Array}  arr Array with numbers
 *
 * @return {Number}     Product of numbers in array (1 if no numbers are given)
 */
function multiply (arr) {
    var result = 1;

    arr.forEach(function (num) {
        result *= (isNaN(parseInt(num))) ? 1 : parseInt(num);
    });

    return result;
}

/**
 * Reverses given string
 *
 * @param  {String} string String to be reversed
 *
 * @return {String}        Reversed string
 */
function reverse (string) {
    return string.split('').reverse().join('');
}

/**
 * Translates given english text into swedish.
 *
 * @param  {String} text English text
 *
 * @return {String}      Translation
 */
function englishToSwedish (text) {
    var obj = {
        'merry': 'god',
        'christmas': 'jul',
        'and': 'och',
        'happy': 'gott',
        'new': 'nytt',
        'year': 'ar'
    };

    // matches all words
    return text.replace(/[a-z]+/ig, function (word) {
        var uppercase = false;

        if (word.charAt(0).toLowerCase() !== word.charAt(0)) {
            uppercase = true;
            word = word.toLowerCase();
        }

        // if word is in the object, gets it
        if (word in obj) {
            word = obj[word];
        }

        if (uppercase) {
            word =
                word.charAt(0).toUpperCase() +
                word.substring(1, word.length - 1);
        }

        return word;
    });
}

/**
 * Finds the length of the longest word in given array.
 *
 * @param  {Array}  arr Array of words
 *
 * @return {Number}     Length of longest word
 */
function findLongestWord (arr) {
    var max = 0;

    arr.forEach(function (word) {
        if (word.length > max) {
            max = word.length;
        }
    });

    return max;
}

/**
 * Filters and returns word from array, longer than number specified.
 *
 * @param  {Array}  arr    Array of words
 * @param  {Number} length Number above which wordlength is permitted
 *
 * @return {Array}        Words passed filter
 */
function filterLongWords (arr, length) {
    var result = [];

    arr.forEach(function (word) {
        if (word.length >= length) {
            result.push(word);
        }
    });

    return result;
}

/**
 * Counts ammount each character is used in given string and returns and object
 * with each character's length.
 *
 * @param  {String} str String whose characters will be counted
 *
 * @return {Object}     Object with character count
 */
function charFreq (str) {
    var obj = {};

    str.split('').forEach(function (character) {
        if (character in obj) {
            obj[character]++;
        } else {
            obj[character] = 1;
        }
    });

    return obj;
}

/**
 * Given an amount, returns '<amount> leva', except add '($$$)' at the end if
 * the amount is 1 million.
 *
 * @param  {Number} value Amount
 *
 * @return {String}       Ammount with currency
 */
function checkValue (value) {

    value = (isNaN(parseInt(value))) ? 0 : parseInt(value);

    var string = (value === 1000000) ? ' dollars ($$$)' : ' leva';

    return value + string;
}

/**
 * Switches two strings' first two characters with one another
 *
 * @param  {String} first  First string
 * @param  {String} second Second string
 *
 * @return {String}        Both strings with switched characters, separated by
 *                         a space
 */
function mixUp (first, second) {
    return second.substr(0, 2) +
        first.substr(2, first.length - 2) + ' ' +
        first.substr(0, 2) +
        second.substr(2, second.length - 2);
}

/**
 * Given a string, returns a version where all trailing occurrences of its
 * first character have been replaced with '*'.
 *
 * @param  {String} str String to be modified
 *
 * @return {String}     Modified string
 */
function fixStart (str) {
    var result = [str.charAt(0)];

    str.substr(1, str.length).split('').forEach(
        function (c) {
            if (c === result[0]) {
                c = '*';
            }
            result.push(c);
    });

    return result.join('');
}

/**
 * Given a string, if its length is at least 3, adds 'ing' to its end.
 * Unless it already ends in 'ing', in which case adds 'ly' instead.
 * If the string length is less than 3, leaves it unchanged.
 *
 * @param  {String} str String to be modified
 *
 * @return {String}     Modified string
 */
function verbing (str) {

    if(str.length <= 3) {
        return str;
    }

    if(str.substr(-3, 3) === 'ing') {
        return str + 'ly';
    }

    return str + 'ing';
}

/**
 * Given a string, first appearance of the substring 'not' and 'bad' is found.
 * If 'bad' follows 'not', the whole 'not'...'bad' substring is replaced with
 * 'good' and the result is returned. If 'not' and 'bad' are not found in
 * the right sequence (or at all), the original sentence is returned.
 *
 * @param  {String} str String to be checked
 *
 * @return {String}     Edited string
 */
function notBad (str) {
    return str.replace(/(\s)(not\s+\w+\s+bad)/ig, '$1good');
}

/**
 * A random item is removed from the given array and returned.
 * If the array is empty, null is returned.
 *
 * @param  {Array}            arr  Array with items
 *
 * @return {typeof(Array[#])} Item
 */
function removeRandomItem (arr) {

    if (arr.length === 0) {
        return null;
    }

    var index = Math.floor(Math.random() * arr.length);
    var item = arr[index];

    for (var i = 0, j = arr.length - 1; i <= arr.length; i++, j--) {
        if(j !== index) {
            arr.unshift(arr.pop());
        } else {
            arr.pop();
        }
    }

    return item;
}

/**
 * Reorders the items in an array into a random order
 *
 * @param  {Array} arr Array to be reordered
 *
 * @return {void}      Reorders the given array
 */
function randomizeOrder (arr) {
    var result = [];

    for (var i = 0, j = arr.length; i < j; i++) {
        result.push(removeRandomItem(arr));
    }

    for (i = 0, j = result.length; i < j; i++) {
        arr.unshift(result.pop());
    }
}