/*TASK 2:*/

/*version_1*/
function maxOfThree1(n1, n2, n3){

    var max = n1;

    if(n2 > n1){
        max = n2;
    }

    if(n3 > n2){
        max = n3;
    }

    return max;
}
/*version_2*/
function maxOfThree2(a,b,c){
    return Math.max(a, b, c);
}

/*TASK 3:*/

/*version_1*/
function vowelCheck1(str){

    if(str.length != 1){
        return false;
    }

    var vowels = ['a', 'e', 'i', 'o', 'u', 'y'];

    return vowels.indexOf(str.toLowerCase()) >= 0;
}

/*version_2*/
function vowelCheck2(str){

    var vowels = ['a', 'o', 'e', 'i', 'y'];

    if(str.length==1 && vowels.indexOf(str.toLowerCase()) >=0){
        return true;
    }

    return false;
}

/*TASK 4:*/
/*version_1*/
function translate(str){

var result = '';

for (var i = 0; i < str.length; i++) {
    if(!vowelCheck2(str[i]) && str[i].match(/[a-z]/i)){
        result += str[i] + 'o' + str[i];
    } else{
        result += str[i];
    }
}
/*version_2 - by Samuil*/

function translate (string) {
    var result = [],
    vowels = 'aeouiy'

    string.split('').forEach (function(character){
        if (vowels.indexOf(character) === -1 && character.test('/[a-z]/i')) {
            character = character + 'o' + character;
        }
        result.push(character)
    });
    return.join('')
}


/*TASK 5:*/
function sum(arr){

    var result = 0;

    for (var i = 0; i < arr.length; i++) {
        result += (!isNaN(parseInt(arr[i]))) ? parseInt(arr[i]) : 0;
    }

    return result;
}

function multiply(arr){

    var result = 1;

    for (var i = 0; i < arr.length; i++) {
        result *= (!isNaN(parseInt(arr[i]))) ? parseInt(arr[i]) : 1;
    }

    return result;
}