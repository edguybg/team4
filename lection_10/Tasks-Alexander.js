/* Task 3 */
function checkVowels(str) {
	var vowelArray = ['a','o','e','i','y','u'];

	if (str.length == 1 && vowelArray.indexOf(str.toLowerCase()) >= 0) {
		return true;
	}else {
		return false;
	}
}
/*Task 4*/
function translate(str) {
	var result = '';

	for (var i = 0; i < str.length; i++) {
		if (!checkVowels(str[i]) && str[i].match(/[a-z]/i)) {
			result += str[i] + 'o' + str[i];
		} else {
			result += str[i];
		}
	}

	return result;
}
/*Task 5*/
function sumArray(arr) {
	var sum = 0;

	for (var i = 0; i < arr.length; i++) {
		sum += parseInt(arr[i]);
	}

	return sum;
}

function multiplyArray(arr) {
	var multiply = 1;

	for (var i = 0; i < arr.length; i++) {
		multiply *= parseInt(arr[i]);
	}

	return multiply;
}
/*Task 6*/
function reverse(str) {

	return str.split('').reverse().join('');
}
/*Task 7*/
var transWords = {
		"merry" : "god",
		"christmas": "jul",
	    "and": "och",
	    "happy": "gott",
	    "new":"nytt",
	    "year": "år"
}

function translate(sentence) {
	var result = [],
		sentence = sentence.toLowerCase(),
		words = sentence.split(' ');

	for (var i = 0; i < words.length; i++) {
		result.push(transWords[words[i]]);
	}

	return result.join(' ');
}

/*Task 8*/
function findLongestWord(arr) {
	var wordLength = 0,
		longestWord;

	for (var i = 0; i < arr.length; i++) {
		if (arr[i].length >  wordLength) {
				var  wordLength = arr[i].length;
				longestWord = arr[i];
		}
	}

	return longestWord;
}

/*Task 9*/
function filterLongWords(wordArr, wordLength) {
	var wordArray = [];

	for(var i = 0; i < wordArr.length; i++) {
			if (wordArr[i].length > wordLength) {
				wordArray.push(wordArr[i]);
			}
	}

	return wordArray;
}

/*Task 10*/
function charFreq(str) {
	var freq = {};

	for (var i=0; i < str.length; i++) {
		var character = str.charAt(i);

		if (freq[character]) {
			freq[character]++;
		} else {
			freq[character] = 1;
		}
	}

	return freq;
}

/*Task 11*/
function returnAmount(amount) {
	var result;

	if (amount > 0 ) {
		result = amount + ' leva';
		if (amount == 1000000) {
			result = amount + ' dollars ($$$)';
		}
	}

	return result;
}

/*Task 12*/
function mixUp(word1, word2) {
		var stringArray = [];

		if (word1.length > 2 && word2.length > 2) {
				var replaced1 = word2.charAt(0) + word2.charAt(1) + word1.substr(2, word1.length),
					replaced2 = word1.charAt(0) + word1.charAt(1) + word2.substr(2, word2.length);

				stringArray.push(replaced1);
				stringArray.push(replaced2);
		}

		return stringArray;
}

/*Task 13*/
function fixStart(str) {
	var newString,
		firstLetter = str.charAt(0);

	if (str.length > 1) {
		for (i = 0; i < str.length; i++) {
			if (i != 0 && str[i] == firstLetter) {
				newString += "*";
			} else {
				newString += str[i];
			}
		}
	}

	return newString;
}

/*Task 14*/
function verbing(str) {

	if (str.length > 3) {
		if (str.substr(str.length-3, str.length) == 'ing') {
			str += "ly";
		} else {
			str += "ing";
		}
	}

	return str;
}

/*Task 15*/
function notbad(str) {

	if (str.indexOf('not') < str.indexOf('bad')) {
		str = str.substr(0, str.indexOf('not')) + "good";
	}

	return str;
}

/*Task 16*/
function removeRandomItem(arr) {
	
	if (arr.length === 0) {
		return null;
	}

	var randomIndex = Math.floor(Math.random() * (arr.length)),
		item = arr.splice(randomIndex, 1);

	return item;
}

/*Task 17*/
function shuffleArray(arr) {
	var count = arr.length, 
		randomIndex,
		temp;

	while(count > 0) {
		randomIndex = Math.floor(Math.random() * count);
		count--;
		temp = arr[count];
		arr[count] = arr[randomIndex];
		arr[randomIndex] = temp;
	}

	return arr;
}


